.preloaderBody {
  width: 100vw;
  height: 100%;
  background: url('~assets/images/preloader_cover.jpg') no-repeat;
  background-position: 50% 50%;
  background-size: 100% 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 3;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  @media (orientation: portrait) {
    background: url('~assets/images/preloader_cover_portaine.png') no-repeat;
    background-position: 50% 50%;
    background-size: 100% 100%;
  }
}

.preloaderLogo {
  width: 40vw;
  height: 25vw;
  margin-top: 10vh;
  @media (orientation: portrait) {
    width: 90vw;
    height: 55vw;
    margin-top: 32vh;
  }
}

.loaderWrapper {
  width: 90%;
  height: 50px;
  margin: 0 auto;
  top: 65%;
  left: 5%;
  position: absolute;
  overflow: hidden;
}

.loaderBorder {
  background-image: url('~assets/images/loaderStart.png'),
    url('~assets/images/loaderCenter.png'), url('~assets/images/loaderEnd.png');
  background-repeat: no-repeat, no-repeat, no-repeat;
  background-size: auto 50px, calc(100% - 69px) 100%, auto 50px;
  background-position-x: 0, 35px, calc(100% - 0px);
  width: 100%;
  height: 100%;
  position: relative;
  z-index: 3;
}

.loaderProgress {
  position: absolute;
  top: 28%;
  left: 20px;
  width: 0%;
  height: 22px;
  z-index: 3;
}
