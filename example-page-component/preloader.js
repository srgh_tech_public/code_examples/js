import React, { Component } from 'react';
import styles from './preloader.module.scss';

import loaderAnim from 'assets/images/load_anim.png';
import newLogo from 'assets/images/logoStatic.png';

export default class Preloader extends Component {
  state = {
    loadProcess: 0,
    isShow: true
  };

  componentDidMount() {
    this.intervalLoader = setInterval(() => {
      this.setState({ loadProcess: this.state.loadProcess + 0.01 }, () => {
        if (this.state.loadProcess >= 2) {
          clearInterval(this.intervalLoader);
          this.setState({ isShow: false });
          if (this.props.callback) {
            this.props.callback();
          }
        }
      });
    }, 100);
  }

  componentDidUpdate(pProps, pState) {
    if (pProps.hideWithDelay !== this.props.hideWithDelay) {
      clearInterval(this.intervalLoader);
      this.intervalLoader = setInterval(() => {
        this.setState({ loadProcess: this.state.loadProcess + 0.01 }, () => {
          if (this.state.loadProcess >= 2) {
            clearInterval(this.intervalLoader);
            this.setState({ isShow: false });
            if (this.props.callback) {
              this.props.callback();
            }
          }
        });
      }, 10);
    }
  }

  componentWillUnmount() {
    clearInterval(this.intervalLoader);
  }

  render() {
    const { loadProcess, isShow } = this.state;
    const { orientation } = this.props;

    const endPercent = orientation === 'portaine' ? 88 : 94;
    const styleLoad = {
      width: `${(loadProcess > 2 ? 1 : loadProcess / 2) * endPercent}%`
    };
    return (
      <>
        {isShow ? (
          <div className={styles.preloaderBody}>
            <img className={styles.preloaderLogo} src={newLogo} alt="" />
            <div className={styles.loaderWrapper}>
              <div className={styles.loaderBorder}></div>
              <img
                className={styles.loaderProgress}
                src={loaderAnim}
                style={styleLoad}
                alt=""
              />
            </div>
          </div>
        ) : null}
      </>
    );
  }
}
