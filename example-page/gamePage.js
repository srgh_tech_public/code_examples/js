import React, { Component } from 'react';
import withOrientationIndicator from 'hoc/orientationBoundry.hoc';
import withScroller from 'hoc/scrollBoundry.hoc';
import styles from './gamePage.module.scss';
import { WhiteButton, Preloader } from 'components';
import { AUDIO_TOGGLE } from 'actions/audio.actions';
import { GAMES_SET_GAME_ORIENTATION } from 'actions/games.actions';
import cx from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
class GamePage extends Component {
  state = {
    loadProcess: 0,
    isMenuOpen: false,
    hidenPreloader: false,
    load: true
  };
  componentDidMount() {
    window.addEventListener('message', this.iFrameMessageRecieve);
  }
  iFrameMessageRecieve = e => {
    if (e.data === 'loadingComplete') {
      this.setState({ hidenPreloader: true });
    };
    if (e.data.message === 'app.loaded') {
      this.removePreloader = setTimeout(() => this.setState({ hidenPreloader: true }), 1200);
    }
  };
  componentWillUnmount() {
    this.props.setGameOrientation(false);
    window.removeEventListener('message', this.iFrameMessageRecieve);
    clearTimeout(this.removePreloader);
  }
  menuToggler = () => {
    this.setState({ isMenuOpen: !this.state.isMenuOpen });
  };
  render() {
    const {
      gameData: { activeGame = '' },
      history,
      appHeigth,
      orientation,
      t
    } = this.props;
    let showMenu = true;
    try {
      const gameUrl = new URL(activeGame);
      if (gameUrl.hostname === 'assets.ferrari888-service.com') {
        showMenu = false;
      }
    } catch (err) {}

    const { isMenuOpen, hidenPreloader, load } = this.state;
    return (
      <div className={styles.gamePage} style={{ height: `${appHeigth}px` }}>
        <Preloader
          orientation={orientation}
          hideWithDelay={hidenPreloader}
          callback={() => {
            this.setState({ load: false });
          }}
        />

        {activeGame ? (
          <iframe
            src={activeGame}
            title={'game'}
            frameBorder="0"
            className={cx(
              styles.gamePage__frame,
              load ? styles.gamePage__frame_hiden : null
            )}
            style={{
              width: `${window.innerWidth}px`,
              height: `${appHeigth}px`
            }}
            width={window.innerWidth}
            height={appHeigth}
          ></iframe>
        ) : null}

        {showMenu && (
          <div
            className={cx(
              styles.gamePage__menuWrapper,
              isMenuOpen ? styles.gamePage__menuWrapper_active : null
            )}
          >
            <div
              className={cx(
                styles.gamePage__sideMenu,
                window.orientation > 0
                  ? styles.gamePage__sideMenu_small
                  : styles.gamePage__sideMenu_big
              )}
            >
              <div
                className={cx(
                  styles.gamePage__sideMenuToggler,
                  isMenuOpen ? styles.gamePage__sideMenuToggler_active : null
                )}
                onClick={this.menuToggler}
              ></div>

              <WhiteButton
                text={t('AUDIO')}
                className={cx(
                  styles.gamePage__whiteButton,
                  styles.gamePage__whiteButton_blackText
                )}
                textStyle={{
                  fontSize: `13px`,
                  color: `#350e27`,
                  WebkitTextFillColor: 'black',
                  WebkitTextStroke: '0'
                }}
                onClick={this.props.audioToggle}
              />
              <WhiteButton
                text={t('LOBBY')}
                className={cx(
                  styles.gamePage__whiteButton,
                  styles.gamePage__whiteButton_blackText
                )}
                textStyle={{
                  fontSize: `13px`,
                  color: `#350e27`,
                  WebkitTextFillColor: 'black',
                  WebkitTextStroke: '0'
                }}
                onClick={() => {
                  history.push('/lobby');
                }}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => state;
const mapDispatchToProps = {
  audioToggle: AUDIO_TOGGLE,
  setGameOrientation: GAMES_SET_GAME_ORIENTATION
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  withOrientationIndicator(
    withScroller(withRouter(withTranslation()(GamePage)))
  )
);
