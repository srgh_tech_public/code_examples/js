import { USERDATA_DISPATCH_TOKEN } from '../actions/userdata.actions';

export const storageMiddleware = ({ dispatch }) => next => action => {
  next(action);
  switch (action.type) {
    case 'STORAGE_ADD_TOKEN': {
      sessionStorage.setItem('token', JSON.stringify({ token: action.token }));
      dispatch(USERDATA_DISPATCH_TOKEN(action.token, true));
      break;
    }
    case 'STORAGE_RECEIVE_TOKEN': {
      const token = sessionStorage.getItem('token');
      dispatch(USERDATA_DISPATCH_TOKEN(token, token ? true : false));
      break;
    }
    case 'STORAGE_REMOVE_TOKEN': {
      sessionStorage.removeItem('token');
      dispatch(USERDATA_DISPATCH_TOKEN(null, false));
      break;
    }
    case 'STORAGE_SAVE_GAME_HISTORY': {
      const nextGame = action.payload;
      const prevGames = JSON.parse(localStorage.getItem('gamesHistory'));
      const nextGames = prevGames
        ? prevGames.filter(game => game.id !== nextGame.id).slice(0, 24)
        : [];
      localStorage.setItem(
        'gamesHistory',
        JSON.stringify([nextGame, ...nextGames])
      );
      break;
    }
    default: {
    }
  }
};
