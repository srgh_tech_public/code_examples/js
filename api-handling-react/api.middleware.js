import { API_SUCCESS, API_ERROR } from 'actions/api.actions';
import { STORAGE_REMOVE_TOKEN as removeToken } from 'actions/storage';
import { MODAL_OPEN as openModal } from 'actions/modal.actions';

export const ApiMiddleware = ({ getState, dispatch }) => next => action => {
  next(action);
  if (action.type.includes('API_REQUEST')) {
    const {
      userData: { token }
    } = getState();
    const {
      type,
      endpoint,
      url: externalUrl,
      method,
      returnReqInfo,
      withoutToken
    } = action.meta;

    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      ...(token && !withoutToken
        ? {
            Authorization: `Bearer ${token}`
          }
        : null)
    };

    let url = externalUrl
      ? externalUrl
      : // : `${process.env.REACT_APP_SERVER_URL}api/ext/web`;
        `${window.location.origin}/api/ext/web`;
    let options = {
      method,
      headers,
      ...(method !== 'GET'
        ? {
            body: action.payload ? JSON.stringify(action.payload) : null
          }
        : null)
    };

    fetch(`${url}/${endpoint}`, options)
      .then(resp => {
        if (!resp.ok) {
          throw resp;
        }
        return resp.json();
      })
      .then(data => {
        dispatch(
          API_SUCCESS(type, data, returnReqInfo ? action.payload : null)
        );
      })
      .catch(err => {
        // console.log(err);
        if (err.status === 403) {
          dispatch(removeToken());
          dispatch(
            openModal({
              modalType: 'error',
              text: 'Account already in use',
              title: 'Disconnect'
            })
          );
        }
        return dispatch(API_ERROR(type, err));
      });
  }
};
