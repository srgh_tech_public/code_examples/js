import Event from '../../utils/event';
import eEventIds from './eEventIds';

export default class ControllerWs {
  constructor() {
    this.events = [];
    for (let id in eEventIds) {
      this.events[eEventIds[id]] = new Event();
    }

    this.connect();
  }

  connect = () => {
    this.socket = new WebSocket('wss://teekey.xyz:8082/websocket');

    this.getParams = new URLSearchParams(window.location.search);

    const authKey = Math.round(Math.random() * 1000).toString();

    let accessToken = this.getParams.get('access_token');
    const platformUIID = this.getParams.get('platform_uuid');

    this.socket.addEventListener('open', () => {
      console.log('Socket opened', new Date());
      this.socket.send(
        JSON.stringify({ id: 'auth', authKey, accessToken, platformUIID })
      );

      this.socket.send(JSON.stringify({ id: 'check' }));
    });

    this.socket.addEventListener('close', (event) => {
      console.error('Socket closed', event, new Date());
      setTimeout(() => this.connect(), 5000);
    });

    this.socket.addEventListener('message', this.onMessageReceive);
  };

  onMessageReceive = (event) => {
    const data = JSON.parse(event.data);

    if (!data.id) {
      console.error('Unknown id for ws message: ', data);
      if (data.message) {
        window.controllerModals.modalError.show(data.message);
        window.controllerModals.modalSearchTable.hide();
      }
      return;
    }
    this.events[data.id].call(data);
  };

  send(data) {
    this.socket.send(JSON.stringify(data));
  }

  addEventListener(id, listener) {
    this.events[id].add(listener);
  }

  removeEventListener(id, listener) {
    this.events[id].remove(listener);
  }
}
