import { GameObjects, Display } from 'phaser';
import { CStateReels, CReelMsg } from 'Constants';

const framesNames = {
  '10Card': '10.png',
  ACard: 'a.png',
  KCard: 'k.png',
  '9Card': '9.png',
  JCard: 'j.png',
  QCard: 'q.png',
  Horsesea: 'horsesea.png',
  Cloun: 'cloun.png',
  Chest: 'chest.png',
  Turtle: 'turtles.png',
  Star: 'star.png',
  Dolphine: 'dolphin1.png',
};

export default class Reel extends GameObjects.Container {
  stop = true;

  resultFrames = [];

  #currentIt = 0;

  #lastResultY = null;

  #nextItrReels = [];

  #isTouched = false;

  state = CStateReels.RDY;

  #index = null;

  constructor({ scene, x, y, width, height, parent, frameList, index }) {
    super(scene);

    // init container
    this.scene = scene;
    this.scene.add.existing(this);
    this.setSize(width, height);

    this.step = this.displayHeight / 12;
    this.count = 0;
    this.x = x;
    this.y = y;
    this.#index = index;
    this.allFrames = this.scene.add.group();
    // init elements and gamesObjects
    this.frameSizeValue = {
      width: this.displayWidth,
      height: this.displayHeight / 3,
    };

    this.initBg();
    this.initMask(parent);
    this.initReelFrames(frameList);
    this.initFadeObject();
    this.initInteractive();
  }

  initBg() {
    this.ReelBg = this.scene.add.sprite(0, 0, 'Slot', 'back_slot.png');
    this.ReelBg.displayWidth = this.width;
    this.ReelBg.displayHeight = this.height;
    this.add(this.ReelBg);
    this.sendToBack(this.ReelBg);
  }

  initReelFrames = (newFrameArray) => {
    let lastElY = this.displayHeight;

    this.allFrames.children.each((frame) => {
      lastElY = Math.min(frame.y, lastElY) === frame.y ? frame.y : lastElY;
    });
    newFrameArray.forEach((frameKey, index) => {
      const framePositionY =
        lastElY -
        this.frameSizeValue.height -
        this.frameSizeValue.height * index;
      const frame = this.scene.add.sprite(
        0,
        framePositionY,
        'Slot',
        framesNames[frameKey]
      );
      frame.displayWidth = this.displayWidth * 0.99;
      frame.displayHeight = this.frameSizeValue.height;
      frame.itr = this.#currentIt;
      this.allFrames.add(frame);
      this.add(frame);
    });
  };

  initReelResult = (resArray) => {
    // detect and remove hidden elements
    let lastHidenElY = -this.frameSizeValue.height;
    this.allFrames.children.each((frame) => {
      if (frame.y <= 0) {
        lastHidenElY =
          Math.max(frame.y, lastHidenElY) === frame.y ? frame.y : lastHidenElY;
      }
    });
    this.allFrames.children.each((frame) => {
      if (frame.y < lastHidenElY) {
        frame.destroy();
      }
    });

    // init result frames
    this.initReelFrames(resArray);
  };

  initMask(parent) {
    this.maskShape = this.scene.make.graphics();
    this.maskShape.fillRect(
      parent.x + this.x * parent.scaleX,
      parent.y + this.y * parent.scaleY,
      this.width * parent.scaleX,
      this.height * parent.scaleY
    );
    if (this.mask) this.clearMask();
    this.setMask(new Display.Masks.GeometryMask(this, this.maskShape));
  }

  initFadeObject() {
    this.fadeObj = this.scene.make.graphics();
    this.fadeObj.fillStyle('0x000000', 0.3);
    this.fadeObj.fillRect(0, 0, this.width, this.height);
    this.fadeObj.visible = false;
    this.add(this.fadeObj);
    this.bringToTop(this.fadeObj);
  }

  initInteractive() {
    this.setInteractive();
    this.input.hitArea.x = this.width / 2;
    this.input.hitArea.y = this.height / 2;
    // todo: rework
    this.on('pointerup', this.onEndHandler);
  }

  updateState = (newState, stateData) => {
    this.state = newState;
    switch (newState) {
      case CStateReels.RDY: {
        // console.log('rdy');
        break;
      }
      case CStateReels.DONE: {
        // console.log('done');
        break;
      }
      case CStateReels.INIT_RES: {
        this.#lastResultY = this.displayHeight;
        this.initReelResult(stateData.resArray);

        this.allFrames.children.each((frame) => {
          this.#lastResultY =
            Math.min(frame.y, this.#lastResultY) === frame.y
              ? frame.y
              : this.#lastResultY;
        });
        this.#nextItrReels = stateData.newReel;
        this.updateState(CStateReels.INIT_NEW_SLOTS);
        break;
      }
      case CStateReels.INIT_NEW_SLOTS: {
        this.#currentIt += 1;
        this.initReelFrames(this.#nextItrReels);
        this.updateState(CStateReels.SHOW_RES);

        break;
      }
      case CStateReels.SHOW_RES: {
        this.showResAnimation();
        break;
      }
      default: {
        // console.warn('error');
        break;
      }
    }
  };

  rotateReel = (step = this.step) => {
    this.allFrames.incY(step);
    this.allFrames.children.each((frame) => {
      if (frame.y >= this.displayHeight) {
        if (frame.itr === this.#currentIt) {
          let lastElY = frame.y;
          this.allFrames.children.each((child) => {
            lastElY =
              Math.min(child.y, lastElY) === child.y ? child.y : lastElY;
          });
          frame.y = lastElY - this.frameSizeValue.height;
        } else {
          frame.destroy();
        }
      }
    });
  };

  showResAnimation = () => {
    // todo: need refactor this
    setTimeout(() => {
      if (this.#isTouched) {
        this.bringToTop(this.fadeObj);
        this.fadeObj.visible = true;
      }
    }, 800);

    this.animationEnd = this.scene.add.tween({
      targets: this.allFrames.getChildren(),
      y: `+=${Math.abs(this.#lastResultY)}`,
      ease: 'Back.Out',
      duration: 800,
      repeat: 0,
      onComplete: () => {
        this.updateState(CStateReels.DONE);
      },
    });
  };

  clearForNewGame = () => {
    if (this.#isTouched) {
      this.#isTouched = false;
      this.fadeObj.visible = false;
    }
    this.updateState(CStateReels.RDY);
  };

  sendMsgToScene = (msg, payload) => {
    this.scene.reelMsgHandler(msg, payload);
  };

  resizeContainer() {
    this.initMask(this.parentContainer);
  }

  onEndHandler = (e) => {
    if (e.getDistanceY() > 0 && e.velocity.y > -5) {
      if (!this.#isTouched) this.sendMsgToScene(CReelMsg.T_START_ALL);
    } else {
      if (this.#isTouched && this.state === CStateReels.LAUNCHED) {
        this.sendMsgToScene(CReelMsg.T_STOP_ONE, { index: this.#index });
      }
      if (!this.#isTouched && this.state === CStateReels.RDY) {
        this.#isTouched = true;
        this.updateState(CStateReels.LAUNCHED);
        this.sendMsgToScene(CReelMsg.T_START_ONE);
      }
    }
  };
}
