import { GameObjects } from 'phaser';
import { gradientGenerator, btnEventsFactory } from 'helpers';
import { CArrGradients as grad } from 'Constants';

export default class BtnWidget extends GameObjects.Container {
  constructor({ scene, x, y, width, height, header, fixedZero = false }) {
    super(scene);

    this.scene = scene;
    this.scene.add.existing(this);
    this.setSize(width, height);
    this.x = x;
    this.y = y;

    this.textConfig = {
      header,
      fixedZero,
      fsName: this.width * 0.1,
      fsValue: this.width * 0.11,
      ar: this.scene.scale.gameSize.aspectRatio,
    };

    this.initPlusBtn();
    this.initMinusBtn();
    this.initTxtBg();
    this.initWidgetName();
    this.initWidgetValue();
  }

  initPlusBtn = () => {
    this.btnPlus = this.scene.add
      .sprite(
        this.displayWidth * 0.51 - 2,
        this.displayHeight * 0.38,
        'BtnField',
        'bt_plus.png'
      )
      .setInteractive();
    this.btnPlus.displayWidth = this.width / 2;
    this.btnPlus.displayHeight = this.height * 0.6;

    btnEventsFactory({
      btn: this.btnPlus,
      onFrame: 'bt_plus_on.png',
      offFrame: 'bt_plus.png',
      callbackUp: () => {
        // todo: connect bet ctrl
      },
    });
    this.add(this.btnPlus);
  };

  initMinusBtn = () => {
    this.btnMinus = this.scene.add
      .sprite(0, this.displayHeight * 0.38, 'BtnField', 'bt_minus.png')
      .setInteractive();

    this.btnMinus.displayWidth = this.width / 1.97;
    this.btnMinus.displayHeight = this.height * 0.6;

    btnEventsFactory({
      btn: this.btnMinus,
      onFrame: 'bt_minus_on.png',
      offFrame: 'bt_minus.png',
      callbackUp: () => {
        // todo: connect bet ctrl
      },
    });

    this.add(this.btnMinus);
  };

  initTxtBg = () => {
    this.txtBg = this.scene.add.sprite(0, 0, 'BtnField', 'back_lines.png');
    this.txtBg.displayWidth = this.width;
    this.txtBg.displayHeight = this.height * 0.4;
    this.add(this.txtBg);
  };

  initWidgetName = () => {
    this.widgetName = this.scene.add.text(
      this.displayWidth * 0.03,
      this.displayHeight * 0.02,
      this.textConfig.header,
      {
        font: `400 ${this.textConfig.fsName}px Verdana`,
        color: '#ffffff',
      }
    );
    this.widgetName.setResolution(this.textConfig.ar);
    gradientGenerator(this.widgetName, grad.arrColorsWhite);
    this.add(this.widgetName);
  };

  initWidgetValue = () => {
    this.widgetValue = this.scene.add.text(
      this.displayWidth * 0.95,
      this.displayHeight * 0.01,
      0,
      {
        font: `bold ${this.textConfig.fsValue}px Verdana right`,
        color: '#ffffff',
      }
    );

    this.widgetValue.setResolution(this.textConfig.ar);
    this.widgetValue.setOrigin(1, 0);
    gradientGenerator(this.widgetValue, grad.arrColorsWhite);
    this.add(this.widgetValue);
  };

  adaptiveResolution(size) {
    this.txt.setResolution(size.aspectRatio);
    this.txtScore.setResolution(size.aspectRatio);
  }

  updateValue = (newValue) => {
      this.widgetValue = newValue;
  };
}
